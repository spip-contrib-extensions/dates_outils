# Dates outils
Des outil pour la gestion des dates.

Des filtres et critères qui peuvent faciliter la vie. En grande partie empruntés au
plugin [Agenda](https://contrib.spip.net/Agenda-2-0)

Inséréz les fichiers correpondants dans votre fichier `mes_fonctions.php` 

````
 // Les filtres de dates_outils.
 include_spip('filtres/inc_agenda_filtres');
 include_spip('filtres/agenda_fonctions');

 // Les critères de dates_outils.
 include_spip('criteres/inc_agenda_filtres');
````

