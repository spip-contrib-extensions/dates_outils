<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP
// extrait automatiquement de https://trad.spip.net/tradlang_module/paquet-dates_outils?lang_cible=nl
// ** ne pas modifier le fichier **

if (!defined('_ECRIRE_INC_VERSION')) {
	return;
}

$GLOBALS[$GLOBALS['idx_lang']] = array(

	// D
	'dates_outils_description' => 'Voegt verschillende tools voor het beheer van datums als functiecriteria samen, soms ontleend aan andere plugins zoals agenda.',
	'dates_outils_nom' => 'Datumhulpmiddelen (Dates outils)',
	'dates_outils_slogan' => 'Hulpmiddelen voor het beheer van datums'
);
