<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP
// extrait automatiquement de https://trad.spip.net/tradlang_module/dates_outils?lang_cible=nl
// ** ne pas modifier le fichier **

if (!defined('_ECRIRE_INC_VERSION')) {
	return;
}

$GLOBALS[$GLOBALS['idx_lang']] = array(

	// C
	'champ_annee_label' => 'Jaar:',
	'champ_date_debut_label' => 'Begindatum:',
	'champ_date_fin_label' => 'Einddatum:',
	'champ_date_label' => 'Datum:',
	'champ_jour_label' => 'Dag:',
	'champ_mois_label' => 'Maand:',

	// D
	'date_au' => 'op',
	'date_du' => 'van',
	'dates_outils_titre' => 'Datumhulpmiddelen (Dates outils)',

	// I
	'info_date_debut' => 'begindatum',
	'info_date_fin' => 'einddatum',

	// N
	'nuits' => 'nachten',

	// P
	'periode_label' => 'Periode:'
);
