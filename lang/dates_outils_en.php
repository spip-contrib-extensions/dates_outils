<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP
// extrait automatiquement de https://trad.spip.net/tradlang_module/dates_outils?lang_cible=en
// ** ne pas modifier le fichier **

if (!defined('_ECRIRE_INC_VERSION')) {
	return;
}

$GLOBALS[$GLOBALS['idx_lang']] = array(

	// C
	'champ_annee_label' => 'Year:',
	'champ_date_debut_label' => 'from:',
	'champ_date_fin_label' => 'to:',
	'champ_date_label' => 'Date:',
	'champ_jour_label' => 'Day:',
	'champ_mois_label' => 'Month:',

	// D
	'date_au' => 'on',
	'date_du' => 'from',
	'dates_outils_titre' => 'Date tools',

	// I
	'info_date_debut' => 'from',
	'info_date_fin' => 'to',

	// N
	'nuits' => 'nights',

	// P
	'periode_label' => 'Periode:'
);
