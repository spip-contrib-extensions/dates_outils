<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP
// Fichier source, a modifier dans https://git.spip.net/spip-contrib-extensions/dates_outils.git
if (!defined('_ECRIRE_INC_VERSION')) {
	return;
}

$GLOBALS[$GLOBALS['idx_lang']] = array(

	// C
	'champ_annee_label' => 'Année :',
	'champ_date_debut_label' => 'Date début :',
	'champ_date_fin_label' => 'Date fin :',
	'champ_date_label' => 'Date :',
	'champ_jour_label' => 'Jour :',
	'champ_mois_label' => 'Mois :',

	// D
	'date_au' => 'au',
	'date_du' => 'du',
	'dates_outils_titre' => 'Dates outils',

	// I
	'info_date_debut' => 'date début',
	'info_date_fin' => 'date fin',

	// N
	'nuits' => 'nuits',

	// P
	'periode_label' => 'Période :'
);
